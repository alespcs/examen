import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the PostDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-details',
  templateUrl: 'post-details.html',
})
export class PostDetailsPage {
  private items: any;
  private user: any;
  private name: string;
  private photo: any;
  private thumbnailUrl: any;
  private comments: any;
  private totalComments: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public servicesProvider: ServicesProvider,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController
  ) {
    this.items = navParams.get('item');
    this.servicesProvider.getUser(this.items.userId).then(data => {
      this.user = JSON.parse(data['_body']);
      this.name = this.user[0].name;
    });
    this.servicesProvider.getMedia(this.items.id).then(data => {
      this.photo = JSON.parse(data['_body']);
      this.thumbnailUrl = this.photo[0].thumbnailUrl;
    });

    this.servicesProvider.getComments(this.items.id).then(data => {
      this.comments = JSON.parse(data['_body']);
      this.totalComments = this.comments.length;
    });

  }


  public openComments(event, id) {
    const profileModal = this.modalCtrl.create('CommentsPage', { IdPost: id });
    profileModal.present();
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PostDetailsPage');
  }



}
