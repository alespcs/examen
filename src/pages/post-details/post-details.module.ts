import { NgModule } from '@angular/core';
import { IonicPage, IonicPageModule } from 'ionic-angular';
import { PostDetailsPage } from './post-details';

@IonicPage()
@NgModule({
  declarations: [
    PostDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PostDetailsPage),
  ],
})
export class PostDetailsPageModule { }
