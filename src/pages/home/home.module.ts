import { NgModule } from '@angular/core';
import { IonicPage, IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { HttpModule } from '@angular/http';

@IonicPage()
@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    HttpModule
  ],
})
export class HomePageModule { }
