import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  private items: any;
  private list: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private servicesProvider: ServicesProvider
  ) {
  }

  ionViewDidLoad() {
    this.getList();
  }

  /**
   * Este metodo filtra los datos ingresados en el buscador
  */

  public filterPosts(event: any) {
    let val = event.target.value;
    // Se consulta el servicio y se obtienen todos los post
    this.servicesProvider.getTodos().then(data => {
      this.list = JSON.parse(data['_body']);
    });
    // se filtra la entrada del input del buscador con la lista del servicio cosultado 
    this.items = this.list.filter((item) => {
      return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1)
    });

  }

  /**
   * Este metodo obtine la lista de todos los post
   */
  public getList() {
    this.servicesProvider.getTodos().then(data => {
      this.items = JSON.parse(data['_body']);
    });
  }


  /**
   * Este metodo consulta el usuario por el id del usuario
   * @param userID recibe el id del usuario
   */
  public getUser(userID) {
    this.servicesProvider.getTodos().then(data => {
      this.items = JSON.parse(data['_body']);
      console.log(this.items);
    });
  }

  /**
   * Este método envia mediante push a otra página para visualizar los detalles de post
   * @param event recibe el evento desde el metodo
   * @param item es una variable que envia el objeto a la pagina que recibe el push
   */
  public viewsPosts(event, item: any) {
    this.navCtrl.push('PostDetailsPage', {
      item: item
    });
  }


}
