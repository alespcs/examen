import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {
  private comments: any;
  private IdPost: any;
  private totalComments: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public servicesProvider: ServicesProvider) {
    this.IdPost = this.navParams.get('IdPost');
    this.servicesProvider.getComments(this.IdPost).then(data => {
      this.comments = JSON.parse(data['_body']);
      this.totalComments = this.comments.length;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
