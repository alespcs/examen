import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from '../../config';


/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
/**
 * Clase que contiene la interación con el API End Point
 */
export class ServicesProvider {
  headers: Headers;
  options: RequestOptions;
  public params: URLSearchParams = new URLSearchParams();
  public option = new RequestOptions({ headers: this.headers, search: this.params });

  constructor(public http: Http) {
    console.log('Hello ServicesProvider Provider');
  }

  /**
   * Este método consulta todos los posts
   */
  public getTodos() {
    return new Promise((resolve, reject) => {
      this.http.get(Config.API_ENDPOINT + 'posts', this.option)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  /**
   * Este método consulta todos los usuarios filtrado por el id 
   */
  public getUser(id) {
    return new Promise((resolve, reject) => {
      this.http.get(Config.API_ENDPOINT + 'users?id=' + id)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  /**
   * Este método consulta todos las imaganes correspondientes a cada post
   * @param id esta variable es el id del post a consultar
   */
  public getMedia(id) {
    return new Promise((resolve, reject) => {
      this.http.get(Config.API_ENDPOINT + 'photos?postId=' + id)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  /**
   * Este método consulta todos los comentarios, pertenecientes a un post
   * @param id esta variable es el id del post a consultar
   */
  public getComments(id) {
    return new Promise((resolve, reject) => {
      this.http.get(Config.API_ENDPOINT + 'posts/' + id + '/comments')
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
