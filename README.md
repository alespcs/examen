Este aplicación es desarrollada con Ionic V3 y Angular V4

## Para utilizar la aplicación es necesario instalar las librerias requeridas

    * NodeJS V6 o superior
    * Ionic 
    * Cordova
    * json-server
#### Primero clonar el este repositorio
```bash
$ git clone git@gitlab.com:alespcs/examen.git
$ cd examen
```
#### Segundo si no ha instalado Ionic de forma global
```bash
$ sudo npm install -g ionic cordova
```
#### Si tiene instalado Ionic y cordova
```bash
$ sudo npm install
```
#### Luego agregar la plataforma para Android
```bash
$ ionic cordova platform add android
```
### Seguido se debe instalar json-server
```bash
$ npm install -g json-server
```
### Seguido se debe instalar json-server global
```bash
$ npm install -g json-server
```
### Levantar el servidor 
```bash
$ json-server --watch db.json --port 3000
```

#### Cambiar la configuración en el archivo src/config.ts
la constante **API_ENDPOINT** por la url del servidor json-server

#### Para ejecutar el proyecto en la pc se debe ejecutar
```bash
$ ionic serve --lab
```



